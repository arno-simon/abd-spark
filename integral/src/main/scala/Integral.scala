/* Integral.scala */
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

object Integral {

  def f(x:Double) = 1/x

  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Integral");
    val spark = new SparkContext(conf);

    val slices = if (args.length > 0) args(0).toInt else 2;
    val N = if (args.length > 1) args(1).toInt else 1000;

    val a = 1f;
    val b = 10f;
    val h = (b-a)/N;

    val sum = spark.parallelize(0 until N-1, slices).map { i =>
      f(a + i * h);
    }.reduce(_ + _)

    println("--> Integrale de 1/x entre 1 et 10 : " + sum*h);	
  }
}

