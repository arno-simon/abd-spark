/* Digrams.scala */
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf

object Digrams {

  def main(args: Array[String]) {
    val conf = new SparkConf().setAppName("Digrams");
    val spark = new SparkContext(conf);
    val slices = if (args.length > 0) args(0).toInt else 1;
   
   	// load file
    val textFile = spark.textFile("miserables.txt", slices);
    
    
    // split text to sentences
    val sentences = textFile.flatMap(line => line.split("\\.|\\?|\\!"));

    // split sentences to words, replace some specials characters and slide by 2 the map
   	val bigrams = sentences.flatMap(word => word.split("\\s").map(x => x.replaceAll("\\,","")).sliding(2))
		   				.map{ words =>  
		   					if(words.length == 2) ((words(0), words(1)), 1)
							else (("",""), 1)
		   				} // create digrams with a value of 1
		   				.reduceByKey(_ + _)	 // count same digrams
		   				.sortBy(_._2, false); // sort by value in descending order   	
   	// write results
    bigrams.saveAsTextFile("/home/arno/results");
  }
}

